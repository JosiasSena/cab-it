/*
 * @author Xun Cui
 * May be used for user data management, can be adjust according for real condition
 */
package com.jsquareddevelopment.cabit;

import java.util.ArrayList;

public class Users {
	String nameString;
	String passwordString;
	ArrayList<String> favoritesArrayList;
	ArrayList<String> historyArrayList;

	// Create a null user;
	public Users() {

	}

	// Create a registered user with name and password;
	public Users(String nameString, String passwordString) {
		this.nameString = nameString;
		this.passwordString = passwordString;
	}

	// Create a registered user with favorites and without history;
	public Users(String nameString, String passwordString,
			ArrayList<String> favoritesArrayList) {
		this.nameString = nameString;
		this.passwordString = passwordString;
		this.favoritesArrayList = favoritesArrayList;
	}

	// Create a registered user with history;
	public Users(String nameString, String passwordString,
			ArrayList<String> favoritesArrayList,
			ArrayList<String> historyArrayList) {
		this.nameString = nameString;
		this.passwordString = passwordString;
		this.favoritesArrayList = favoritesArrayList;
		this.historyArrayList = historyArrayList;
	}

	// set password;
	public void setPassword(String passwordString) {
		this.passwordString = passwordString;
	}

	// get favorites;
	public ArrayList<String> getFavorites() {
		return this.favoritesArrayList;
	}

	public ArrayList<String> getHistory() {
		return this.historyArrayList;
	}

	// Clear the user's favorites;
	public void clearFavorites() {
		this.favoritesArrayList = null;
	}

	// Clear the user's history;
	public void clearHistory() {
		this.historyArrayList = null;
	}

}
