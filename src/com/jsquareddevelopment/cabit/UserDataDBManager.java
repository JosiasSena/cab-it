/*
 * @author Xun Cui
 * database manager 
 * to manage data with insert and delete methods
 */
package com.jsquareddevelopment.cabit;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class UserDataDBManager {

	private final static String DATABASE_NAME = "USERSDATA.db";
	private final static int DATABASE_VERSION = 1;
	private final static String FAVORITEADDRESS_TABLE = "favoritesAddress_table";
	public final static String FAVORITESADDRESS_COLUMN = "favoritesAddress_column";
	private final static String HISTORY_TABLE = "history_table";
	private final static String HISTORY_COLUMN = "history_column";
	private UserDataDBHelper dbHelper;
	private SQLiteDatabase db;

	//constructor 
	public UserDataDBManager(UserDataDBHelper dbHelper, SQLiteDatabase db) {
		this.dbHelper = dbHelper;
		this.db = db;
	}

	

	// delete data;
	public void deletefavorites(String address) {
		String sql = "delete from " + FAVORITEADDRESS_TABLE + " where "
				+ FAVORITESADDRESS_COLUMN + " = " + "'" + address + "'";
		db.execSQL(sql);
	}

	// add favorite address;
	public void insertfavorites(String address) {
		// initialize an instance to store data
		if (!isAddressExist(address)) {
			ContentValues cv = new ContentValues();
			cv.put(FAVORITESADDRESS_COLUMN, address); // add favorites address
			db.insert(FAVORITEADDRESS_TABLE, null, cv);// implement the insert  order
														

		}
	}
	//avoid add an exist address 
	public boolean isAddressExist(String address) {
		Cursor c = db.rawQuery("select * from " + FAVORITEADDRESS_TABLE
				+ " where " + FAVORITESADDRESS_COLUMN + " = ?",
				new String[] { address });
		if (c.moveToFirst()) {
			return true;
		} else {
			return false;
		}
	}

}
