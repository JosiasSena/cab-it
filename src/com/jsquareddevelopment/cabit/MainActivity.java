package com.jsquareddevelopment.cabit;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map.Entry;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.R.integer;
import android.app.Activity;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SearchView;
import android.widget.SearchView.OnQueryTextListener;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMapClickListener;
import com.google.android.gms.maps.GoogleMap.OnMarkerDragListener;
import com.google.android.gms.maps.GoogleMap.OnMyLocationButtonClickListener;
import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class MainActivity extends ActionBarActivity implements
		NavigationDrawerFragment.NavigationDrawerCallbacks {

	private NavigationDrawerFragment mNavigationDrawerFragment;

	/**
	 * Used to store the last screen title. For use in
	 * {@link #restoreActionBar()}.
	 */
	private CharSequence mTitle;

	TextView counter1, counter2, counter3;
	Button minusCounter1, minusCounter2, minusCounter3;
	Button plusCounter1, plusCounter2, plusCounter3;
	static ImageButton chooseCarLayoutButton;
	static ImageButton requestTaxiButton;
	static RelativeLayout relativeLayout;
	static ListView myListView;
	static ActionBar actionBar;
	static FragmentManager fm;
	static Window getWindow;
	static Activity activity;

	public static TextView navFullName, navPhoneNumber, navAddress;
	public static String fullName, mPhoneNumber, mAddress;

	public static String adres;
	static SearchView searchLocation;
	SQLiteDatabase db;
	UserDataDBHelper dbHelper;
	UserDataDBManager dbManager;

	private static final String TAG_EMAIL = "email";
	private static final String TAG_FNAME = "firstName";
	private static final String TAG_LNAME = "lastName";
	private static final String TAG_ADDRESS = "address";
	private static final String TAG_PHONE = "phoneNumber";
	private static final String TAG_CITY = "city";
	private static final String TAG_STATE = "state";
	private static final String TAG_ZIP = "zip";

	private static JSONArray mEmails = new JSONArray();
	private static ArrayList<HashMap<String, String>> mEmailList;
	private static final String READ_EMAILS_URL = "http://cabitus.com/app/getinformation.php";

	public static String passedEmail;

	public MainActivity() {
		super();
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		mNavigationDrawerFragment = (NavigationDrawerFragment) getSupportFragmentManager()
				.findFragmentById(R.id.navigation_drawer);
		mTitle = getTitle();

		// Set up the drawer.
		mNavigationDrawerFragment.setUp(R.id.navigation_drawer,
				(DrawerLayout) findViewById(R.id.drawer_layout));

		fm = getSupportFragmentManager();

		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			passedEmail = extras.getString("Email");
		}

		new LoadInfo().execute();
	}

	@Override
	public void onNavigationDrawerItemSelected(int position) {
		// update the main content by replacing fragments
		FragmentManager fragmentManager = getSupportFragmentManager();
		fragmentManager
				.beginTransaction()
				.replace(R.id.container,
						PlaceholderFragment.newInstance(position + 1)).commit();
	}

	public void onSectionAttached(int number) {

	}

	public void restoreActionBar() {
		actionBar = getSupportActionBar();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
		actionBar.setDisplayShowTitleEnabled(true);
		actionBar.setTitle(mTitle);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		if (!mNavigationDrawerFragment.isDrawerOpen()) {
			// Only show items in the action bar relevant to this screen
			// if the drawer is not showing. Otherwise, let the drawer
			// decide what to show in the action bar.
			getMenuInflater().inflate(R.menu.main, menu);
			restoreActionBar();
			SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);

			/** Search View Start */
			searchLocation = (SearchView) menu.findItem(R.id.search_action)
					.getActionView();
			searchLocation.setSearchableInfo(searchManager
					.getSearchableInfo(getComponentName()));

			searchLocation.setOnQueryTextListener(new OnQueryTextListener() {

				@Override
				public boolean onQueryTextSubmit(String query) {
					return false;
				}

				@Override
				public boolean onQueryTextChange(String newText) {
					// TODO Auto-generated method stub
					return false;
				}
			});

			/** Search View Finish */

			return true;
		}
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.

		switch (item.getItemId()) {
		case R.id.signOut_action:
			Intent loginActivityIntent = new Intent(
					"com.jsquareddevelopment.cabit.LOGIN");
			loginActivityIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			startActivity(loginActivityIntent);
			Toast.makeText(this, "Signed out succesfully.", Toast.LENGTH_SHORT)
					.show();
			finish();
			break;
		case R.id.addToFavorites_action:
			//@author: Xun Cui
			// EditText searchLocationEditText =
			// (EditText)findViewById(R.id.search_action);
			// if(searchLocation.getQuery().toString().equals("") ||
			// searchLocation)
			adres = searchLocation.getQuery().toString();
			Log.e("favoriteAddress", adres);
			//if the input is valid add address to favorites
			if (testNotNull(adres)) {
				Log.e("favoriteAddressFromSearchView", adres);
				Toast.makeText(MainActivity.this,
						"Address Added to Favorites! Please refresh",
						Toast.LENGTH_SHORT).show();
				// create database here, and insert the favorites string in to
				// database
				dbHelper = new UserDataDBHelper(this);
				db = dbHelper.getWritableDatabase();
				dbHelper.onCreate(db);
				dbManager = new UserDataDBManager(dbHelper, db);
				dbManager.insertfavorites(adres);

				// clear the insert string to avoid add it again we refreash
				// after
				// delete it
				adres = null;
				break;
			} else {
				//Tell users that the address is not valid.
				Toast.makeText(MainActivity.this,
						"Address is not a valid address， Please enter again",
						Toast.LENGTH_SHORT).show();
				break;
			}

		case R.id.search_action:
			//@author: Xun Cui
			//do not complete yet, can do this according previous one 
			String addressString = searchLocation.getQuery().toString();
			Log.e("history address: ", addressString);
			if (addressString != null && addressString != "") {
				SharedPreferences historyPreferences = getSharedPreferences(
						null, MODE_PRIVATE);
				SharedPreferences.Editor editor2 = historyPreferences.edit();
				editor2.putString("historyAddress", addressString);
				editor2.commit();
			}
			break;
		case R.id.profile_action:
			Intent profileActivityIntent = new Intent(
					"com.jsquareddevelopment.cabit.EDITPROFILE");
			profileActivityIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			startActivity(profileActivityIntent);
			break;
		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}


	@Override
	public void onBackPressed() {
		Log.d("CDA", "onBackPressed Called");
		// Intent setIntent = new Intent(Intent.ACTION_MAIN);
		// setIntent.addCategory(Intent.CATEGORY_HOME);
		// setIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		// startActivity(setIntent);
		// finish();
	}

	public static class PlaceholderFragment extends Fragment implements
			OnMapClickListener, LocationListener, OnClickListener, Runnable {

		private static final String ARG_SECTION_NUMBER = "section_number";

		public static SupportMapFragment mMapFragment = SupportMapFragment
				.newInstance();
		GoogleMap map = mMapFragment.getMap();
		GoogleMapOptions options = new GoogleMapOptions();

		TextView counter1, counter2, counter3, addressOnMainScreen;
		Button minusCounter1, minusCounter2, minusCounter3;
		Button plusCounter1, plusCounter2, plusCounter3;

		public static Geocoder addressGeocoder;
		LocationRequest mLocationRequest;
		LocationClient mLocationClient;
		Marker marker;

		// The minimum distance to change Updates in meters
		private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 10; // 10
																		// meters

		// The minimum time between updates in milliseconds
		private static final long MIN_TIME_BW_UPDATES = 3500; // 35 Seconds

		// Milliseconds per second
		private static final int MILLISECONDS_PER_SECOND = 1000;

		// Update frequency in seconds
		public static final int UPDATE_INTERVAL_IN_SECONDS = 3;

		// Update frequency in milliseconds
		private static final long UPDATE_INTERVAL = MILLISECONDS_PER_SECOND
				* UPDATE_INTERVAL_IN_SECONDS;

		// The fastest update frequency, in seconds
		private static final int FASTEST_INTERVAL_IN_SECONDS = 1;

		// A fast frequency ceiling in milliseconds
		private static final long FASTEST_INTERVAL = MILLISECONDS_PER_SECOND
				* FASTEST_INTERVAL_IN_SECONDS;

		private static final int TWO_MINUTES = 1000 * 60 * 2;

		// Returns a new instance of this fragment for the given section number.
		public static PlaceholderFragment newInstance(int sectionNumber) {
			PlaceholderFragment fragment = new PlaceholderFragment();
			Bundle bundle = new Bundle();
			bundle.putInt(ARG_SECTION_NUMBER, sectionNumber);
			fragment.setArguments(bundle);
			return fragment;
		}

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {

			View rootView = inflater.inflate(R.layout.fragment_main, container,
					false);
			setRefs(rootView); // Set references
			run(); // load map
			return rootView;
		}

		public void setRefs(View view) {
			relativeLayout = (RelativeLayout) view
					.findViewById(R.id.chooseCarLayout);

			counter1 = (TextView) view.findViewById(R.id.counter1);
			counter2 = (TextView) view.findViewById(R.id.counter2);
			counter3 = (TextView) view.findViewById(R.id.counter3);
			addressOnMainScreen = (TextView) view
					.findViewById(R.id.addressOnMainScreen);

			minusCounter1 = (Button) view.findViewById(R.id.minusCounter1);
			minusCounter2 = (Button) view.findViewById(R.id.minusCounter2);
			minusCounter3 = (Button) view.findViewById(R.id.dropUsAmessage);

			chooseCarLayoutButton = (ImageButton) view
					.findViewById(R.id.chooseCarLayoutButton);
			requestTaxiButton = (ImageButton) view
					.findViewById(R.id.requestTaxiButton);

			plusCounter1 = (Button) view.findViewById(R.id.plusCounter1);
			plusCounter2 = (Button) view.findViewById(R.id.plusCounter2);
			plusCounter3 = (Button) view.findViewById(R.id.plusCounter3);

			minusCounter1.setOnClickListener(this);
			minusCounter2.setOnClickListener(this);
			minusCounter3.setOnClickListener(this);
			plusCounter1.setOnClickListener(this);
			plusCounter2.setOnClickListener(this);
			plusCounter3.setOnClickListener(this);
			chooseCarLayoutButton.setOnClickListener(this);
			requestTaxiButton.setOnClickListener(this);
		}

		@Override
		public void onDestroy() {
			super.onDestroy();
		}

		@Override
		public void onPause() {
			Fragment fragment = (getFragmentManager()
					.findFragmentById(R.id.map));
			if (fragment.isResumed()) {
				getFragmentManager().beginTransaction().remove(fragment)
						.commit();
			}
			super.onPause();
		}

		@Override
		public void onResume() {
			setUpMapIfNeeded();
			super.onResume();
		}

		/**
		 * Determines whether one Location reading is better than the current
		 * Location fix
		 * 
		 * @param location
		 *            The new Location that you want to evaluate
		 * @param currentBestLocation
		 *            The current Location fix, to which you want to compare the
		 *            new one
		 */
		protected boolean isBetterLocation(Location location,
				Location currentBestLocation) {
			if (currentBestLocation == null) {
				// A new location is always better than no location
				return true;
			}

			// Check whether the new location fix is newer or older
			long timeDelta = location.getTime() - currentBestLocation.getTime();
			boolean isSignificantlyNewer = timeDelta > TWO_MINUTES;
			boolean isSignificantlyOlder = timeDelta < -TWO_MINUTES;
			boolean isNewer = timeDelta > 0;

			// If it's been more than two minutes since the current location,
			// use the new location
			// because the user has likely moved
			if (isSignificantlyNewer) {
				return true;
				// If the new location is more than two minutes older, it must
				// be worse
			} else if (isSignificantlyOlder) {
				return false;
			}

			// Check whether the new location fix is more or less accurate
			int accuracyDelta = (int) (location.getAccuracy() - currentBestLocation
					.getAccuracy());
			boolean isLessAccurate = accuracyDelta > 0;
			boolean isMoreAccurate = accuracyDelta < 0;
			boolean isSignificantlyLessAccurate = accuracyDelta > 200;

			// Check if the old and new location are from the same provider
			boolean isFromSameProvider = isSameProvider(location.getProvider(),
					currentBestLocation.getProvider());

			// Determine location quality using a combination of timeliness and
			// accuracy
			if (isMoreAccurate) {
				return true;
			} else if (isNewer && !isLessAccurate) {
				return true;
			} else if (isNewer && !isSignificantlyLessAccurate
					&& isFromSameProvider) {
				return true;
			}
			return false;
		}

		/** Checks whether two providers are the same */
		private boolean isSameProvider(String provider1, String provider2) {
			if (provider1 == null) {
				return provider2 == null;
			}
			return provider1.equals(provider2);
		}

		@Override
		public void onAttach(Activity activity) {
			super.onAttach(activity);
			((MainActivity) activity).onSectionAttached(getArguments().getInt(
					ARG_SECTION_NUMBER));
		}

		@Override
		public void onMapClick(LatLng arg0) {

		}

		@Override
		public void onLocationChanged(Location location) {
			// Called when a new location is found by the network location
			// provider.
			double longi = location.getLongitude();
			double lati = location.getLatitude();

			Log.e("LONG", longi + "");
			Log.e("LAT", lati + "");
		}

		@Override
		public void onProviderDisabled(String provider) {

		}

		@Override
		public void onProviderEnabled(String provider) {
		}

		@Override
		public void onStatusChanged(String provider, int status, Bundle extras) {
		}

		@Override
		public void onLowMemory() {
			super.onLowMemory();
		}

		public List<Address> getAddress(double latit, double longi) {
			try {
				Geocoder geocoder = new Geocoder(getActivity()
						.getApplicationContext(), Locale.getDefault());
				List<Address> addresses = geocoder.getFromLocation(latit,
						longi, 1);
				if (addresses != null) {
					String address = addresses.get(0).getAddressLine(0);
					String city = addresses.get(0).getAddressLine(1);
					addressOnMainScreen.setText(address + " " + city);
					return addresses;
				} else {
					return null;
				}
			} catch (IOException e) {
				e.printStackTrace();
				Toast.makeText(getActivity().getApplicationContext(),
						"Could not get address..!", Toast.LENGTH_LONG).show();
				addressOnMainScreen.setText("No location found..");
				addressOnMainScreen.setText("No location found..");
				Toast.makeText(getActivity().getApplicationContext(),
						"Could not get address..!", Toast.LENGTH_LONG).show();
				addressOnMainScreen.setText("No location found..");
				return null;
			}
		}

		// Here we are creating the map
		public void map() {

			MapsInitializer.initialize(getActivity().getApplicationContext());

			switch (GooglePlayServicesUtil
					.isGooglePlayServicesAvailable(getActivity()
							.getApplicationContext())) {
			case ConnectionResult.SUCCESS:

				mLocationRequest = LocationRequest.create();
				mLocationRequest
						.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
				mLocationRequest.setInterval(UPDATE_INTERVAL);
				mLocationRequest.setFastestInterval(FASTEST_INTERVAL);

				// Gets to GoogleMap from the map and initializes
				if (map != null) {
					setUpMap();
				} else {
					// log shows that mapView is null
					Log.d("map(): ", "map is null");
				}
				break;
			case ConnectionResult.SERVICE_MISSING:
				Toast.makeText(getActivity(), "SERVICE MISSING",
						Toast.LENGTH_SHORT).show();
				break;
			case ConnectionResult.SERVICE_VERSION_UPDATE_REQUIRED:
				Toast.makeText(getActivity(), "UPDATE REQUIRED",
						Toast.LENGTH_SHORT).show();
				break;
			default:
				Toast.makeText(
						getActivity(),
						GooglePlayServicesUtil
								.isGooglePlayServicesAvailable(getActivity()),
						Toast.LENGTH_SHORT).show();
			}
		}

		public void setUpMap() {
			Toast.makeText(getActivity(), "Connected", Toast.LENGTH_SHORT)
					.show();

			// Enabling MyLocation Layer of Google Map
			map.setMyLocationEnabled(true);

			// Google Maps Options
			options.mapType(GoogleMap.MAP_TYPE_NORMAL).compassEnabled(true)
					.rotateGesturesEnabled(false).tiltGesturesEnabled(false);

			map.animateCamera(CameraUpdateFactory.zoomTo(17.5f));

			// Ui Settings
			map.getUiSettings().setMyLocationButtonEnabled(true);
			map.getUiSettings().setScrollGesturesEnabled(true);
			map.getUiSettings().setCompassEnabled(false);
			map.getUiSettings().setZoomControlsEnabled(false);

			map.animateCamera(CameraUpdateFactory.zoomTo(17.5f));

			// Creating a criteria object to retrieve provider
			Criteria criteria = new Criteria();

			criteria.setPowerRequirement(Criteria.POWER_LOW);
			criteria.setAccuracy(Criteria.ACCURACY_FINE);
			criteria.setAltitudeRequired(false);
			criteria.setBearingRequired(false);
			criteria.setSpeedAccuracy(Criteria.ACCURACY_HIGH);

			// Getting LocationManager object from System Service
			LocationManager locationManager = (LocationManager) getActivity()
					.getApplicationContext().getSystemService(LOCATION_SERVICE);

			// Getting Current Location
			// final Location location =
			// locationManager.getLastKnownLocation("network");
			final Location location = locationManager
					.getLastKnownLocation("gps");

			locationManager.requestLocationUpdates(
					LocationManager.GPS_PROVIDER, MIN_TIME_BW_UPDATES,
					MIN_DISTANCE_CHANGE_FOR_UPDATES, this);

			if (location != null) {
				new Thread(new Runnable() {
					public void run() {
						getActivity().runOnUiThread(new Runnable() {
							@Override
							public void run() {
								onLocationChanged(location);
								// isBetterLocation(location, location);
							}
						});
					}
				}).start();
			}

			// Get current location Latitude and Longitude
			double latitude = location.getLatitude();
			double longitude = location.getLongitude();

			// Create LatLng object
			final LatLng latLng = new LatLng(latitude, longitude);

			// get address
			getAddress(latitude, longitude);

			// Show the current location on map
			map.moveCamera(CameraUpdateFactory.newLatLng(latLng));

			// Zoom in the map
			map.animateCamera(CameraUpdateFactory.zoomTo(17.5f));

			// Creates Current Location Map Marker
			marker = map.addMarker(new MarkerOptions()
					.position(latLng)
					.draggable(true)
					.icon(BitmapDescriptorFactory
							.defaultMarker(BitmapDescriptorFactory.HUE_GREEN))
					.title("You are here"));
			marker.showInfoWindow();

			map.setOnMarkerDragListener(new OnMarkerDragListener() {

				@Override
				public void onMarkerDragStart(Marker m) {

				}

				@Override
				public void onMarkerDragEnd(Marker m) {
				}

				@Override
				public void onMarkerDrag(Marker m) {
					// TODO make marker smooth
					if (m.isDraggable()) {
						LatLng dragPosition = m.getPosition();
						map.moveCamera(CameraUpdateFactory
								.newLatLng(dragPosition));
						double dragLat = dragPosition.latitude;
						double dragLong = dragPosition.longitude;
						getAddress(dragLat, dragLong);
					}
				}
			});

			marker.showInfoWindow();

			final CameraPosition cameraPosition = new CameraPosition(latLng,
					17.5f, 10, 10);

			map.setOnMyLocationButtonClickListener(new OnMyLocationButtonClickListener() {
				@Override
				public boolean onMyLocationButtonClick() {
					map.animateCamera(CameraUpdateFactory
							.newCameraPosition(cameraPosition));
					marker.setPosition(latLng);
					getAddress(latLng.latitude, latLng.longitude);
					return false;
				}
			});
		}

		@Override
		public void onClick(View view) {
			int total1 = Integer.parseInt(counter1.getText().toString());
			int total2 = Integer.parseInt(counter2.getText().toString());
			int total3 = Integer.parseInt(counter3.getText().toString());

			switch (view.getId()) {
			case R.id.minusCounter1:
				if (total1 > 0) {
					total1--;
					counter1.setText("" + total1);
				} else {
					counter1.setText("" + 0);
				}
				break;
			case R.id.minusCounter2:
				if (total2 > 0) {
					total2--;
					counter2.setText("" + total2);
				} else {
					counter2.setText("" + 0);
				}
				break;
			case R.id.dropUsAmessage:
				if (total3 > 0) {
					total3--;
					counter3.setText("" + total3);
				} else {
					counter3.setText("" + 0);
				}
				break;
			case R.id.plusCounter1:
				total1++;
				counter1.setText("" + total1);
				break;
			case R.id.plusCounter2:
				total2++;
				counter2.setText("" + total2);
				break;
			case R.id.plusCounter3:
				total3++;
				counter3.setText("" + total3);
				break;
			case R.id.chooseCarLayoutButton:
				relativeLayout
						.setVisibility(relativeLayout.getVisibility() == View.VISIBLE ? View.GONE
								: View.VISIBLE);
				break;
			case R.id.requestTaxiButton:
				// NOTE if no taxis are selected the default type of taxi should
				// be sedans.

				// TODO Always add what the address the taxi is requested to go
				// to, to the history screen.
				/**
				 * TODO Send request to drivers and taxi HQs of the companies
				 * selected in the "Taxi Companies Screen" - how many cabs are
				 * needed - what kind of cabs are wanted - to what location to
				 * send them to.
				 */

				Toast.makeText(getActivity().getApplicationContext(),
						"Taxi Requested", Toast.LENGTH_SHORT).show();

				if (relativeLayout.getVisibility() == View.VISIBLE) {
					relativeLayout.setVisibility(View.GONE);
				}
				break;
			default:
				break;
			}
		}

		@Override
		public void run() {
			fm = getFragmentManager();
			map = ((SupportMapFragment) getFragmentManager().findFragmentById(
					R.id.map)).getMap();
			map();
		}

		private void setUpMapIfNeeded() {
			// Do a null check to confirm that we have not already instantiated
			// the map.
			if (map == null) {
				map = ((SupportMapFragment) getFragmentManager()
						.findFragmentById(R.id.map)).getMap();
				// Check if we were successful in obtaining the map.
				if (map != null) {
					// The Map is verified. It is now safe to manipulate the
					// map.
					setUpMap();
				}
			}
		}

	}

	// Adapter for Navigation Drawer
	public static class MyAdapter extends ArrayAdapter<Object> implements
			OnItemClickListener {

		ArrayAdapter<String> listAdapter;

		public MyAdapter(Context context, int textViewResourceId,
				ArrayList<Object> objects) {
			super(context, textViewResourceId, objects);
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {

			if (convertView == null) {
				LayoutInflater factory = LayoutInflater.from(this.getContext());
				convertView = factory.inflate(R.layout.mylistadapter, null);
			}
			/*******************************************/
			navFullName = (TextView) convertView.findViewById(R.id.navFullName);
			navPhoneNumber = (TextView) convertView
					.findViewById(R.id.navPhoneNumber);
			navAddress = (TextView) convertView.findViewById(R.id.navAddress);

			navFullName.setText(fullName);
			navPhoneNumber.setText(mPhoneNumber);
			navAddress.setText(mAddress);
			/*******************************************/

			// List View for the navigation drawer
			ListView myListView = (ListView) convertView
					.findViewById(R.id.listMenu);
			String[] menuItems = new String[] { "Home", "Favorites", "History",
					"Taxi Companies", "About Us" };

			ArrayList<String> menuList = new ArrayList<String>();
			menuList.addAll(Arrays.asList(menuItems));

			listAdapter = new ArrayAdapter<String>(getContext(),
					R.layout.simplerow, menuList);

			myListView.setOnItemClickListener(this);

			myListView.setAdapter(listAdapter);
			return convertView;
		}

		@Override
		public void onItemClick(AdapterView<?> av, View view, int position,
				long id) {
			// Navigation Drawer menu click

			FragmentTransaction transaction = fm.beginTransaction();
			switch (position) {
			case 0:
				// Home
				PlaceholderFragment homeFragment = new PlaceholderFragment();
				transaction
						.replace(R.id.container, homeFragment.newInstance(1));
				transaction.commit();
				break;
			case 1:
				// Favorites
				Favorites myfavorites = new Favorites();
				transaction.replace(R.id.container, myfavorites);
				transaction.commit();
				break;
			case 2:
				// History
				History history = new History();
				transaction.replace(R.id.container, history);
				transaction.commit();
				break;
			case 3:
				// Taxi Companies
				TaxiCompanies tc = new TaxiCompanies();
				transaction.replace(R.id.container, tc);
				transaction.commit();
				break;
			case 4:
				// About Us
				AboutUs aboutUs = new AboutUs();
				transaction.replace(R.id.container, aboutUs);
				transaction.commit();
				break;
			default:
				break;
			}

		}

	}

	public static void updateJSONdata() {
		mEmailList = new ArrayList<HashMap<String, String>>();
		JSONParser jParser = new JSONParser();
		JSONObject json = jParser.getJSONFromUrl(READ_EMAILS_URL);

		try {
			mEmails = json.getJSONArray("info");

			// looping through all emails according to the json object returned
			for (int i = 0; i < mEmails.length(); i++) {
				JSONObject c = mEmails.getJSONObject(i);

				// gets the content of each tag
				String email = c.getString(TAG_EMAIL);
				String firstName = c.getString(TAG_FNAME);
				String lastName = c.getString(TAG_LNAME);
				String address = c.getString(TAG_ADDRESS);
				String phoneNumber = c.getString(TAG_PHONE);
				String city = c.getString(TAG_CITY);
				String state = c.getString(TAG_STATE);
				String zip = c.getString(TAG_ZIP);

				// creating new HashMap
				HashMap<String, String> map = new HashMap<String, String>();
				map.put(TAG_EMAIL, email);
				map.put(TAG_FNAME, firstName);
				map.put(TAG_LNAME, lastName);
				map.put(TAG_ADDRESS, address);
				map.put(TAG_PHONE, phoneNumber);
				map.put(TAG_CITY, city);
				map.put(TAG_STATE, state);
				map.put(TAG_ZIP, zip);

				// adding HashList to ArrayList
				mEmailList.add(map);

				for (HashMap<String, String> maps : mEmailList) {
					for (Entry<String, String> mapEntry : maps.entrySet()) {
						String value = mapEntry.getValue();

						if (value.equals(passedEmail)) {
							Log.i("Is this email in the database?", value
									+ " Is in the database!!!");

							int index = mEmailList.indexOf(maps);
							// Log.i("index: ", "" + index);
							// Log.i("mEmailList: ", "" + mEmailList);

							fullName = mEmailList.get(index).get(TAG_FNAME)
									+ " "
									+ mEmailList.get(index).get(TAG_LNAME);

							mPhoneNumber = mEmailList.get(index).get(TAG_PHONE);

							mAddress = mEmailList.get(index).get(TAG_ADDRESS)
									+ " " + mEmailList.get(index).get(TAG_CITY)
									+ " "
									+ mEmailList.get(index).get(TAG_STATE)
									+ " " + mEmailList.get(index).get(TAG_ZIP);
						}
					}
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	public class LoadInfo extends AsyncTask<Void, Void, Boolean> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}

		@Override
		protected Boolean doInBackground(Void... arg0) {
			updateJSONdata();
			return null;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
		}
	}

	//@author: Xun Cui
	// test whether the users' input is empty or not
	// set the minimum valid input not include blank be 3
	public static boolean testNotNull(String address) {
		char[] addresChar = new char[address.length()];
		int count = 0;
		addresChar = address.toCharArray();
		String blank = " ";
		char blankChar = blank.charAt(0);
		for (int i = 0; i < address.length(); i++) {
			if (addresChar[i] != blankChar) {
				count++;
			}

		}
		if (count > 3) {
			return true;
		}
		return false;
	}

}
