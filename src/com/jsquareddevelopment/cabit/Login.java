package com.jsquareddevelopment.cabit;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender.SendIntentException;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.Signature;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.model.GraphUser;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.model.people.Person;
import com.google.android.gms.plus.model.people.Person.Name;

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class Login extends Activity implements OnClickListener,
		GoogleApiClient.ConnectionCallbacks,
		GoogleApiClient.OnConnectionFailedListener, OnItemSelectedListener {

	public TextView emailTextView, passwordTextView, registerTextView;
	public ImageButton signinButton, facebookButton, googleButton;
	public Button forgotPwd;

	String htmlString = "<u>Register</u>";

	Spinner typeSpinner;
	String[] typeStrings = { "Passenger", "Driver" };
	ArrayList<String> arrlist = new ArrayList<String>(20);
	ArrayAdapter<String> typeAdapter;

	private static final int RC_SIGN_IN = 0;
	private GoogleApiClient mGoogleApiClient;

	private boolean mIntentInProgress;
	private static final String TAG = "login";
	private boolean mSignInClicked;
	private ConnectionResult mConnectionResult;

	// Progress Dialog
	private ProgressDialog pDialog;

	// JSON parser class
	JSONParser jsonParser = new JSONParser();

	private static final String LOGIN_URL = "http://www.cabitus.com/app/login.php";

	// JSON element ids from repsonse of php script:
	private static final String TAG_SUCCESS = "success";
	private static final String TAG_MESSAGE = "message";
	
	LocationManager manager;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login);
		getActionBar().hide();

		emailTextView = (TextView) findViewById(R.id.email);
		passwordTextView = (TextView) findViewById(R.id.password);
		registerTextView = (TextView) findViewById(R.id.register);
		signinButton = (ImageButton) findViewById(R.id.signinButton);
		facebookButton = (ImageButton) findViewById(R.id.facebookButton);
		
		
		
		googleButton = (ImageButton) findViewById(R.id.googleButton);
		forgotPwd = (Button) findViewById(R.id.forgotPwd);

		// Spinner
		typeAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, typeStrings);
		typeSpinner = (Spinner) findViewById(R.id.typeSpinner);
		typeSpinner.setAdapter(typeAdapter);
		typeSpinner.setOnItemSelectedListener(this);

		signinButton.setOnClickListener(this);
		facebookButton.setOnClickListener(this);
		
		try {
	        PackageInfo info = getPackageManager().getPackageInfo(
	                "com.jsquareddevelopment.cabit", 
	                PackageManager.GET_SIGNATURES);
	        for (Signature signature : info.signatures) {
	            MessageDigest md = MessageDigest.getInstance("SHA");
	            md.update(signature.toByteArray());
	            Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
	            }
	    } catch (NameNotFoundException e) {

	    } catch (NoSuchAlgorithmException e) {

	    }
		googleButton.setOnClickListener(this);

		registerTextView.setText(Html.fromHtml(htmlString));
		registerTextView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent registerActivityIntent = new Intent(
						"com.jsquareddevelopment.cabit.REGISTER");
				startActivity(registerActivityIntent);
			}
		});

		mGoogleApiClient = new GoogleApiClient.Builder(this)
				.addConnectionCallbacks(this)
				.addOnConnectionFailedListener(this).addApi(Plus.API)
				.addScope(Plus.SCOPE_PLUS_LOGIN).build();

		// Alert dialog
		final LayoutInflater inflater = this.getLayoutInflater();
		final AlertDialog.Builder mDialog = new AlertDialog.Builder(this);
		final View dialogView = inflater.inflate(R.layout.forgetpassword_dialog, null);
		final EditText forgotPWDeditTextView = (EditText) dialogView.findViewById(R.id.forgotPWDtextView);
		
		mDialog.setView(dialogView);
		
		forgotPwd.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
//				mDialog.setView(dialogView);
				mDialog.show();
			}
		});
		
		mDialog.setPositiveButton("Submit", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int id) {
						
						if (isEmailValid(forgotPWDeditTextView.getText().toString())) {
							sendMail("sys-admin@jsquareddevelopment.com",
									"New Password Request", forgotPWDeditTextView.getText().toString() + " needs a new password.");
						}else if (!isEmailValid(forgotPWDeditTextView.getText().toString())) {
							Toast.makeText(Login.this, "Please enter a valid email address.", Toast.LENGTH_SHORT).show();
						}
					}
				});

		mDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
//						mDialog.setView(null);
						dialog.cancel();
						dialog.dismiss();
					}
				});
				
				manager = (LocationManager) getSystemService(LOCATION_SERVICE);
	}
	
	  private void buildAlertMessageNoGps() {
		    final AlertDialog.Builder builder = new AlertDialog.Builder(this);
		    builder.setMessage("Your GPS seems to be disabled, do you want to enable it?")
		           .setCancelable(false)
		           .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
		               public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
		                   startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
		               }
		           })
		           .setNegativeButton("No", new DialogInterface.OnClickListener() {
		               public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
		                    dialog.cancel();
		               }
		           });
		    final AlertDialog alert = builder.create();
		    alert.show();
		}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.signinButton:
			// Logs in
			String email = emailTextView.getText().toString();
			String password = passwordTextView.getText().toString();
			
		    if ( !manager.isProviderEnabled( LocationManager.GPS_PROVIDER ) ) {
		        buildAlertMessageNoGps();
		        
		    }else {
				new AttemptLogin().execute(new BasicNameValuePair("email", email), new BasicNameValuePair("password", password));
			}
			
			break;
		case R.id.facebookButton:
			Session.openActiveSession(this, true, new Session.StatusCallback() {
				@Override
				public void call(Session session, SessionState state,
						Exception exception) {
					if (session.isOpened()) {
						List<String> PERMISSIONS = Arrays.asList("email","user_location");
						if (!session.getPermissions().containsAll(PERMISSIONS)) {
							Session.NewPermissionsRequest newPermissionsRequest = new Session.NewPermissionsRequest(Login.this, PERMISSIONS);
							session.requestNewReadPermissions(newPermissionsRequest);
						}
						Request.newMeRequest(session,
								new Request.GraphUserCallback() {
									@Override
									public void onCompleted(GraphUser user,
											Response response) {
										if (response != null) {
											try {
												
												String first = user.getFirstName();
												String last = user.getLastName();
												String email = user.asMap()
														.get("email")
														.toString();
												String location = user
														.getLocation()
														.getProperty("name")
														.toString();

												Log.e(TAG, " First: " + first
														+ " Last: " + last
														+ " Email: " + email
														+ " Location: "
														+ location 
														 );
												
												
												new AttemptLogin().execute(new BasicNameValuePair("First", first),new BasicNameValuePair("Last", last),new BasicNameValuePair("email", email), new BasicNameValuePair("Location", location));
												Intent mainActivityIntent = new Intent(
														"com.jsquareddevelopment.cabit.MAINACTIVITY");
												startActivity(mainActivityIntent);

												} catch (Exception e) 
													{
												e.printStackTrace();
												Log.d(TAG, "Exception e");
													}
												} 
										
										
										
									}
								}).executeAsync();
					}
				}
				
			});
			break;
		case R.id.googleButton:
			// Uses Google+ to Login
			
				mSignInClicked = true;
				resolveSignInErrors();
			break;
		default:
			break;
		}
	}

	// AsyncTask is a seperate thread than the thread that runs the GUI
	// Any type of networking should be done with asynctask.
	class AttemptLogin extends AsyncTask<NameValuePair, String, String> {

		/* Before starting background thread Show Progress Dialog */
		boolean failure = false;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(Login.this);
			pDialog.setMessage("Attempting to login...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(true);
			pDialog.show();
		}

		@Override
		protected String doInBackground(NameValuePair ... args) {
			// Check for success tag
			int success;
			
			try {
				
				List<NameValuePair> params = new ArrayList<NameValuePair>();
				for(int x=0; x<args.length; x++)
				{
					params.add(args[x]);
				}
				Log.d("request!", "starting");
				// getting product details by making HTTP request
				JSONObject json = jsonParser.makeHttpRequest(LOGIN_URL, "POST", params);
				
				// check your log for json response
				Log.d("Login attempt", json.toString());

				// json success tag
				success = json.getInt(TAG_SUCCESS);
				if (success == 1) {
					Log.d("Login Successful!", json.toString());

					Intent mainActivityIntent = new Intent("com.jsquareddevelopment.cabit.MAINACTIVITY");
					Intent driverActivityIntent = new Intent("com.jsquareddevelopment.cabit.DRIVERHOMESCREEN");

					if (typeSpinner.getSelectedItemPosition() == 0) {
						mainActivityIntent.putExtra("Email", emailTextView.getText().toString());
						startActivity(mainActivityIntent);
					}

					if (typeSpinner.getSelectedItemPosition() == 1) {
						startActivity(driverActivityIntent);
					}

					finish();
					// startActivity(mainActivityIntent);
					return json.getString(TAG_MESSAGE);
				} else {
					Log.d("Login Failure!", json.getString(TAG_MESSAGE));
					return json.getString(TAG_MESSAGE);

				}
			} catch (JSONException e) {
				e.printStackTrace();
			}

			return null;

		}

		protected void onPostExecute(String file_url) {
			// dismiss the dialog once product deleted
			pDialog.dismiss();
			if (file_url != null) {
				//Toast.makeText(Login.this, file_url, Toast.LENGTH_LONG).show();
			}
		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		Session.getActiveSession().onActivityResult(this, requestCode,
				resultCode, data);

		if (requestCode == RC_SIGN_IN) {
			if (resultCode != RESULT_OK) {
				mSignInClicked = false;
			}
			mIntentInProgress = false;
			if (!mGoogleApiClient.isConnecting()) {
				mGoogleApiClient.connect();
			}
		}
	}

	protected void onStart() {
		super.onStart();
		mGoogleApiClient.connect();
	}

	protected void onStop() {
		super.onStop();
		mGoogleApiClient.disconnect();
	}

	public void onConnected(Bundle connectionHint) {
		mSignInClicked = false;
		
		if (Plus.PeopleApi.getCurrentPerson(mGoogleApiClient) != null) {
		    Person currentPerson = Plus.PeopleApi.getCurrentPerson(mGoogleApiClient);
		    
		    String first = currentPerson.getName().getGivenName();
		    String last = currentPerson.getName().getFamilyName();
		    String location = currentPerson.getCurrentLocation();
		    String email = Plus.AccountApi.getAccountName(mGoogleApiClient);

		    Log.e(TAG, " First: " + first
					+ " Last: " + last
					+ " Email: " + email
					+ " Location: "
					+ location 
					 );
		    new AttemptLogin().execute(new BasicNameValuePair("email", email), new BasicNameValuePair("first", first), new BasicNameValuePair("last", last), new BasicNameValuePair("location", location));
		    Intent gloginActivityIntent = new Intent(
					"com.jsquareddevelopment.cabit.MAINACTIVITY");
			startActivity(gloginActivityIntent);
		  }
		

	}

	public void onDisconnected() {
		Log.v(TAG, "Disconnected. Bye!");
	}

	public void onConnectionSuspended(int cause) {
		mGoogleApiClient.connect();
	}

	private void resolveSignInErrors() {
		if (mConnectionResult.hasResolution()) {
			try {
				mIntentInProgress = true;
				startIntentSenderForResult(mConnectionResult.getResolution()
						.getIntentSender(), RC_SIGN_IN, null, 0, 0, 0);
			} catch (SendIntentException e) {

				mIntentInProgress = false;
				mGoogleApiClient.connect();
			}
		}
	}

	public void onConnectionFailed(ConnectionResult result) {
		if (!mIntentInProgress) {
			mConnectionResult = result;
			if (mSignInClicked) {

				resolveSignInErrors();
			}
		}
	}

	@Override
	public void onItemSelected(AdapterView<?> parent, View view, int position,
			long id) {

		position = typeSpinner.getSelectedItemPosition();

		switch (position) {
		case 0:
			// Passenger
			emailTextView.setHint("Email");
			break;
		case 1:
			// Driver
			emailTextView.setHint("ID");
			break;
		default:
			break;
		}
	}

	@Override
	public void onNothingSelected(AdapterView<?> av) {
	}

	/****** Send mail methods ******/
	private static final String username = "sys-admin@jsquareddevelopment.com";
	private static final String password = "yFfoaO,ks}.*";

	private void sendMail(String email, String subject, String messageBody) {
		javax.mail.Session session = createSessionObject();

		try {
			Message message = createMessage(email, subject, messageBody,
					session);
			new SendMailTask().execute(message);
		} catch (AddressException e) {
			e.printStackTrace();
		} catch (MessagingException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	}

	private Message createMessage(String email, String subject,
			String messageBody, javax.mail.Session session)
			throws MessagingException, UnsupportedEncodingException {
		Message message = new MimeMessage(session);
		message.setFrom(new InternetAddress(username, "Cab-it"));
		message.addRecipient(Message.RecipientType.TO, new InternetAddress(
				email, email));
		message.setSubject(subject);
		message.setText(messageBody);
		return message;
	}

	private javax.mail.Session createSessionObject() {
		Properties properties = new Properties();
		properties.put("mail.smtp.auth", "true");
		properties.put("mail.smtp.starttls.enable", "true");
		properties.put("mail.smtp.host", "69.195.124.89"); // Host
		properties.put("mail.smtp.port", "26"); // Port

		return javax.mail.Session.getInstance(properties, new Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		});
	}

	private class SendMailTask extends AsyncTask<Message, Void, Void> {
		private ProgressDialog progressDialog;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progressDialog = ProgressDialog.show(Login.this, "Please wait", "Sending email...", true, false);
		}

		@Override
		protected void onPostExecute(Void aVoid) {
			super.onPostExecute(aVoid);
			progressDialog.dismiss();
			Toast.makeText(Login.this, "New password request sent!", Toast.LENGTH_SHORT).show();
		}

		@Override
		protected Void doInBackground(Message... messages) {
			try {
				Transport.send(messages[0]);
			} catch (MessagingException e) {
				e.printStackTrace();
			}
			return null;
		}
	}

	public static boolean isEmailValid(String email) {
	    boolean isValid = false;

	    String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
	    CharSequence inputStr = email;

	    Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
	    Matcher matcher = pattern.matcher(inputStr);
	    if (matcher.matches()) {
	        isValid = true;
	    }
	    return isValid;
	}
	
}
