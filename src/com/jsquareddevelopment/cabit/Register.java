package com.jsquareddevelopment.cabit;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

public class Register extends Activity {

	EditText firstName, lastName, emailRegister, passwordRegister,
			phoneRegister, addressRegister, cityRegister, stateRegister,
			zipRegister;
	
	public static String fname, lname, email, pass, phone, address, city, state, zip;
	
	ImageButton registerButtton;
	
	 // Progress Dialog
    private ProgressDialog pDialog;

    // JSON parser class
    JSONParser jsonParser = new JSONParser();

    private static final String REGISTER_URL = "http://www.cabitus.com/app/register.php";

    private static final String TAG_SUCCESS = "success";
    private static final String TAG_MESSAGE = "message";
    
    public static JSONObject json;
    
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.register);
		getActionBar().hide();
		
		firstName = (EditText) findViewById(R.id.firstNameRegister);
		lastName = (EditText) findViewById(R.id.lastNameRegister);
		emailRegister = (EditText) findViewById(R.id.emailRegister);
		passwordRegister = (EditText) findViewById(R.id.passwordRegister);
		phoneRegister = (EditText) findViewById(R.id.phoneRegister);
		addressRegister = (EditText) findViewById(R.id.addressRegister);
		cityRegister = (EditText) findViewById(R.id.cityRegister);
		stateRegister = (EditText) findViewById(R.id.stateRegister);
		zipRegister = (EditText) findViewById(R.id.zipRegister);
		
		registerButtton = (ImageButton) findViewById(R.id.registerButtton);
		registerButtton.setOnClickListener( new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				new CreateUser().execute();
			}
		});

	}

	@Override
	public void onBackPressed() {

		Intent registerActivityIntent = new Intent(
				"com.jsquareddevelopment.cabit.LOGIN");
		startActivity(registerActivityIntent);

		super.onBackPressed();
	}

	class CreateUser extends AsyncTask<String, String, String> {

		 /**
         * Before starting background thread Show Progress Dialog
         * */
		boolean failure = false;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(Register.this);
            pDialog.setMessage("Creating User...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

		@Override
		protected String doInBackground(String... args) {
			 // Check for success tag
            int success;
            
            fname = firstName.getText().toString();
            lname = lastName.getText().toString();
            email = emailRegister.getText().toString();
            pass = passwordRegister.getText().toString();
            phone = phoneRegister.getText().toString();
            address = addressRegister.getText().toString();
            city = cityRegister.getText().toString();
            state = stateRegister.getText().toString();    
            zip = zipRegister.getText().toString();   
            
            try {
                // Building Parameters
                List<NameValuePair> params = new ArrayList<NameValuePair>();
                params.add(new BasicNameValuePair("firstname", fname));
                params.add(new BasicNameValuePair("lastname", lname));
                params.add(new BasicNameValuePair("email", email));
                params.add(new BasicNameValuePair("password", pass));
                params.add(new BasicNameValuePair("phonenumber", phone));
                params.add(new BasicNameValuePair("address", address));
                params.add(new BasicNameValuePair("city", city));
                params.add(new BasicNameValuePair("state", state));
                params.add(new BasicNameValuePair("zip", zip));

                Log.d("request!", "starting");

                //Posting user data to script
                json = jsonParser.makeHttpRequest(REGISTER_URL, "POST", params);

                // full json response
                Log.d("Login attempt", json.toString());

                // json success element
                success = json.getInt(TAG_SUCCESS);
                if (success == 1) {
                	Log.d("User Created!", json.toString());
        			Intent mainActivityIntent = new Intent("com.jsquareddevelopment.cabit.LOGIN");
        			finish();
        			startActivity(mainActivityIntent);
                	return json.getString(TAG_MESSAGE);
                }else{
                	Log.d("Login Failure!", json.getString(TAG_MESSAGE));
                	return json.getString(TAG_MESSAGE);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;

		}
		/**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(String file_url) {
            // dismiss the dialog once product deleted
            pDialog.dismiss();
            if (file_url != null){
            	Toast.makeText(Register.this, file_url, Toast.LENGTH_LONG).show();
            }

        }

	}
}