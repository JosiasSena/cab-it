/*
 * @author:Xun Cui
 * help to create database
 * we can add or delete table if we need
 */
package com.jsquareddevelopment.cabit;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public class UserDataDBHelper extends SQLiteOpenHelper {

	private final static String DATABASE_NAME = "USERSDATA.db";
	private final static int DATABASE_VERSION = 1;
	private final static String FAVORITEADDRESS_TABLE = "favoritesAddress_table";
	public final static String FAVORITESADDRESS_COLUMN = "favoritesAddress_column";
	private final static String HISTORY_TABLE = "history_table";
	private final static String HISTORY_COLUMN = "history_column";

	public UserDataDBHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		// TODO Auto-generated constructor stub
	}


	// create table
	public void onCreate(SQLiteDatabase db) {

		String sql = "CREATE TABLE IF NOT EXISTS " + FAVORITEADDRESS_TABLE
				+ " (" + FAVORITESADDRESS_COLUMN + " TEXT);";
		String sql2 = "CREATE TABLE IF NOT EXISTS " + HISTORY_TABLE + " ("
				+ HISTORY_COLUMN + " TEXT);";
		db.execSQL(sql);
		db.execSQL(sql2);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		String sql = "DROP TABLE IF EXISTS " + FAVORITEADDRESS_TABLE;
		String sql2 = "DROP TABLE IF EXISTS" + HISTORY_TABLE;
		db.execSQL(sql);
		db.execSQL(sql2);
		onCreate(db);
	}

}