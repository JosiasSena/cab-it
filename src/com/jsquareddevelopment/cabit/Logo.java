package com.jsquareddevelopment.cabit;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class Logo extends Activity {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.logo);
		getActionBar().hide();

		Thread timer = new Thread() {
			@Override
			public void run() {
				try {
					sleep(1000); // The time it runs for in milliseconds.
									// Example: 3000ms = 3s
				} catch (InterruptedException e) {
					e.printStackTrace();
				} finally {
					Intent loginActivityIntent = new Intent(
							"com.jsquareddevelopment.cabit.LOGIN");
					startActivity(loginActivityIntent);
				}
			}
		};
		timer.start();
	}
}
