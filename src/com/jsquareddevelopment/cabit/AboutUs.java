package com.jsquareddevelopment.cabit;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;

public class AboutUs extends Fragment implements OnClickListener {

	ImageButton dropUsAmessage, instagramImageButton, facebookImageButton,
			twitterImageButton, googleImageButton, linkedInImageButton,
			stumbleUponImageButton;

	public AboutUs() {
	};

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.aboutus_fragment, container,
				false);

		dropUsAmessage = (ImageButton) rootView.findViewById(R.id.dropUsAmessage);
		instagramImageButton = (ImageButton) rootView.findViewById(R.id.instagramImageButton); 	
		facebookImageButton = (ImageButton) rootView.findViewById(R.id.facebookImageButton); 	
		twitterImageButton = (ImageButton) rootView.findViewById(R.id.twitterImageButton); 	
		googleImageButton = (ImageButton) rootView.findViewById(R.id.googleImageButton); 	
		linkedInImageButton = (ImageButton) rootView.findViewById(R.id.linkedInImageButton); 	
		stumbleUponImageButton = (ImageButton) rootView.findViewById(R.id.stumbleUponImageButton); 	
		
		instagramImageButton.setOnClickListener(this);
		facebookImageButton.setOnClickListener(this);
		twitterImageButton.setOnClickListener(this);
		googleImageButton.setOnClickListener(this);
		linkedInImageButton.setOnClickListener(this);
		stumbleUponImageButton.setOnClickListener(this);
		
		// Alert dialog
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle("Choose and Option");
		
		builder.setPositiveButton("Suggestion",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						Intent i = new Intent(
								"com.jsquareddevelopment.cabit.SUGGESTIONS");
						startActivity(i);
					}
				});
		
		builder.setNegativeButton("Bug", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				Intent i = new Intent("com.jsquareddevelopment.cabit.BUGREPORT");
				startActivity(i);
			}
		});

		final AlertDialog dialog = builder.create();

		
		dropUsAmessage.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.show();

			}
		});

		return rootView;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		Intent intent = new Intent(Intent.ACTION_VIEW);
		switch (v.getId()) {
		case R.id.instagramImageButton:
			intent.setData(Uri.parse("http://instagram.com/jsquared_development"));
			startActivity(intent);
			break;
		case R.id.facebookImageButton:
			intent.setData(Uri.parse("https://www.facebook.com/pages/JSquared/266568896836238"));
			startActivity(intent);
			break;
		case R.id.twitterImageButton:
			intent.setData(Uri.parse("https://twitter.com/jSquaredDev"));
			startActivity(intent);
			break;
		case R.id.googleImageButton:
			intent.setData(Uri.parse("https://plus.google.com/u/2/b/109627965048686783185/"));
			startActivity(intent);
			break;
		case R.id.linkedInImageButton:
			intent.setData(Uri.parse("https://www.linkedin.com/company/jsquared-development-corp"));
			startActivity(intent);
			break;
		case R.id.stumbleUponImageButton:
			intent.setData(Uri.parse("http://www.stumbleupon.com/su/1sn0je/www.jsquaredapps.com/"));
			startActivity(intent);
			break;

		default:
			break;
		}
	}

}