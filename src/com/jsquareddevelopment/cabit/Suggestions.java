package com.jsquareddevelopment.cabit;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

public class Suggestions extends Activity implements OnClickListener {

	EditText suggestionDetails;
	ImageButton image1, image2, image3, submitSuggestionButton;
	public static int RESULT_LOAD_IMAGE = 1;
	public static String picturePath;
	File pic;
	List<String> attachment_PathList = new ArrayList<String>();
	Bitmap resizedBitmap;
	String mExtraString;
	final Intent GaleryIntent = new Intent(Intent.ACTION_PICK,
			android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.suggestions);
		setRefs();
	}

	public void setRefs() {
		suggestionDetails = (EditText) findViewById(R.id.suggestionDetails);
		submitSuggestionButton = (ImageButton) findViewById(R.id.submitSuggestionButton);
		image1 = (ImageButton) findViewById(R.id.image1);
		image2 = (ImageButton) findViewById(R.id.image2);
		image3 = (ImageButton) findViewById(R.id.image3);

		image1.setOnClickListener(this);
		image2.setOnClickListener(this);
		image3.setOnClickListener(this);

		submitSuggestionButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				new Thread(new Runnable() {
					public void run() {
						runOnUiThread(new Runnable() {
							@Override
							public void run() {
								if (suggestionDetails.getText().toString()
										.matches("")) {
									Toast.makeText(getApplicationContext(),
											"Please enter details",
											Toast.LENGTH_SHORT).show();
								}
								if (!suggestionDetails.getText().toString()
										.matches("")) {
									sendMail(
											"sys-admin@jsquareddevelopment.com",
											"New Suggestion", suggestionDetails
													.getEditableText()
													.toString());
								}
							}
						});
					}
				}).start();

			}
		});
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
		case R.id.image1:
			Image1();
			break;
		case R.id.image2:
			Image2();
			break;
		case R.id.image3:
			Image3();
			break;
		default:
			break;
		}
	}

	@Override
	protected void onActivityResult(int RequestCode, int ResultCode, Intent Data) {
		super.onActivityResult(RequestCode, ResultCode, Data);

		if (RequestCode == RESULT_LOAD_IMAGE && ResultCode == RESULT_OK
				&& null != Data) {
			Uri SelectedImage = Data.getData();
			String[] FilePathColumn = { MediaStore.Images.Media.DATA };

			Cursor SelectedCursor = getContentResolver().query(SelectedImage,
					FilePathColumn, null, null, null);
			SelectedCursor.moveToFirst();

			int columnIndex = SelectedCursor.getColumnIndex(FilePathColumn[0]);
			picturePath = SelectedCursor.getString(columnIndex);
			attachment_PathList.add(picturePath);

			try {
				resizedBitmap = Bitmap.createScaledBitmap(
						BitmapFactory.decodeFile(picturePath), 140, 140, true);

				if (mExtraString != null) {
					if (getExtra().equals("Image1")) {
						image1.setImageBitmap(resizedBitmap);
					} else if (getExtra().equals("Image2")) {
						image2.setImageBitmap(resizedBitmap);
					} else if (getExtra().equals("Image3")) {
						image3.setImageBitmap(resizedBitmap);
					}

				} else {
					Log.e("Extras", "Intent Extra is null");
				}

			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			}
			SelectedCursor.close();
		}
	}

	/****** Send mail methods start ******/
	private static final String username = "sys-admin@jsquareddevelopment.com";
	private static final String password = "yFfoaO,ks}.*";

	private void sendMail(String email, String subject, String messageBody) {
		javax.mail.Session session = createSessionObject();

		try {
			Message message = createMessage(email, subject, messageBody,
					session);
			new SendMailTask().execute(message);
		} catch (AddressException e) {
			e.printStackTrace();
		} catch (MessagingException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	}

	private Message createMessage(String email, String subject,
			String messageBody, javax.mail.Session session)
			throws MessagingException, UnsupportedEncodingException {

		Multipart multipart = new MimeMultipart("mixed");

		/** Define message */
		BodyPart messageBodyPart = new MimeBodyPart();
		MimeMessage message = new MimeMessage(session);

		message.setFrom(new InternetAddress(username, "New Suggestion"));
		message.addRecipient(Message.RecipientType.TO, new InternetAddress(
				email, email));
		message.setSubject(subject);
		message.setSentDate(new Date());
		message.setText(messageBody);
		messageBodyPart.setContent(messageBody, "text/html");

		multipart.addBodyPart(messageBodyPart);

		/** Handle Attachments */
		for (String str : attachment_PathList) {
			BodyPart attachmentBodyPart = new MimeBodyPart();
			DataSource source = new FileDataSource(str);
			attachmentBodyPart.setDataHandler(new DataHandler(source));
			attachmentBodyPart.setFileName(source.getName());
			multipart.addBodyPart(attachmentBodyPart);
		}

		message.setContent(multipart);
		return message;
	}

	private javax.mail.Session createSessionObject() {
		Properties properties = new Properties();
		properties.put("mail.smtp.auth", "true");
		properties.put("mail.smtp.starttls.enable", "true");
		properties.put("mail.smtp.host", "69.195.124.89"); // Host
		properties.put("mail.smtp.port", "26"); // Port

		return javax.mail.Session.getInstance(properties, new Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		});
	}

	private class SendMailTask extends AsyncTask<Message, Void, Void> {
		private ProgressDialog progressDialog;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progressDialog = ProgressDialog.show(Suggestions.this,
					"Please wait", "Sending suggestion...", true, false);
		}

		@Override
		protected void onPostExecute(Void aVoid) {
			super.onPostExecute(aVoid);
			progressDialog.dismiss();
			Toast.makeText(Suggestions.this, "New suggestion sent!",
					Toast.LENGTH_SHORT).show();
		}

		@Override
		protected Void doInBackground(Message... messages) {
			try {
				Transport.send(messages[0]);
			} catch (MessagingException e) {
				e.printStackTrace();
			}
			return null;
		}
	}

	/****** Send mail methods end ******/

	public void saveExtra(String name) {
		mExtraString = name;
	}

	public String getExtra() {
		return mExtraString;
	}

	public void Image1() {
		new Thread(new Runnable() {
			public void run() {
				runOnUiThread(new Runnable() {
					@Override
					public void run() {
						saveExtra("Image1");
						startActivityForResult(GaleryIntent, RESULT_LOAD_IMAGE);
					}
				});
			}
		}).start();
	}

	public void Image2() {
		new Thread(new Runnable() {
			public void run() {
				runOnUiThread(new Runnable() {
					@Override
					public void run() {
						saveExtra("Image2");
						startActivityForResult(GaleryIntent, RESULT_LOAD_IMAGE);
					}
				});
			}
		}).start();
	}

	public void Image3() {
		new Thread(new Runnable() {
			public void run() {
				runOnUiThread(new Runnable() {
					@Override
					public void run() {
						saveExtra("Image3");
						startActivityForResult(GaleryIntent, RESULT_LOAD_IMAGE);
					}
				});
			}
		}).start();
	}
}