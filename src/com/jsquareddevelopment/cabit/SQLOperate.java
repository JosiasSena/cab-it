package com.jsquareddevelopment.cabit;

public interface SQLOperate {
	public void addUser(UsersData user);
	public void addToFavorites(String favoriteAddress);
	public void deleteFromFavorites(String favoriteAddress);

}
