package com.jsquareddevelopment.cabit;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.Toast;

public class DriverHomeScreen extends ActionBarActivity implements OnClickListener {

	static ActionBar actionBar;
	
	ImageButton driverNavigationButon, driverHistoryButton, driverGasStationsButton, driverEmergencyButton;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.driverhomescreen);
		
		driverNavigationButon = (ImageButton) findViewById(R.id.driverNavigationButon);
		driverHistoryButton = (ImageButton) findViewById(R.id.driverHistoryButton);
		driverGasStationsButton = (ImageButton) findViewById(R.id.driverGasStationsButton);
		driverEmergencyButton = (ImageButton) findViewById(R.id.driverEmergencyButton);
		
		driverNavigationButon.setOnClickListener(this);
		driverHistoryButton.setOnClickListener(this);
		driverGasStationsButton.setOnClickListener(this);
		driverEmergencyButton.setOnClickListener(this);
	}

	public void restoreActionBar() {
		actionBar = getSupportActionBar();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
		actionBar.setDisplayShowTitleEnabled(true);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.driverhomescreenmenu, menu);
		restoreActionBar();
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {
		case R.id.signOutDriver:
			Intent intent = new Intent("com.jsquareddevelopment.cabit.LOGIN");
			startActivity(intent);
			Toast.makeText(this, "Signed out succesfully.", Toast.LENGTH_SHORT).show();
			break;
		default:
			break;
		}
		
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onClick(View v) {

		switch (v.getId()) {
		case R.id.driverEmergencyButton:
			Intent i = new Intent("com.jsquareddevelopment.cabit.DRIVEREMERGENCYACTIVITY");
			startActivity(i);
			break;
		default:
			break;
		}
		
		
	}
}
