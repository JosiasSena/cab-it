package com.jsquareddevelopment.cabit;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.support.v4.app.Fragment;
import android.support.v4.app.ListFragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;

public class History extends ListFragment {

	static ListView listView;
	ArrayList<String> myLines = new ArrayList<String>();
	
	static String[] menuItems = new String[] { "" };
	static String historyString;
	SQLiteDatabase db;
	UserDataDBHelper dbHelper;
	UserDataDBManager dbManager;

	public History() {
	};

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.history_fragment, container,
				false);

		return rootView;

	}
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		// get favorites address from the search bar;
		SharedPreferences favoPreferences = getActivity().getSharedPreferences(
				null, Context.MODE_PRIVATE);
		historyString = favoPreferences.getString("favoAddress", "");
		Log.e("stringFromMainActivity", historyString);// error log;
		dbHelper = new UserDataDBHelper(getActivity());
		db = dbHelper.getWritableDatabase();
		dbHelper.onCreate(db);
		dbManager = new UserDataDBManager(dbHelper, db);
		dbManager.insertfavorites(historyString);

		List<Map<String, Object>> listItems = new ArrayList<Map<String, Object>>();
		for (int i = 0; i < menuItems.length; i++) {
			Map<String, Object> listItem = new HashMap<String, Object>();
			listItem.put("favorites", menuItems[i]);
			listItems.add(listItem);
		}
		
		//List<Map<String, Object>> listItems = new ArrayList<Map<String, Object>>();
		Cursor c = db.rawQuery(
				"SELECT favoritesAddress_column FROM favoritesAddress_table",
				null);
		int columnsSize = c.getColumnCount();
        while (c.moveToNext()) {  
            HashMap<String, Object> map = new HashMap<String, Object>();  
            map.put("favorites", null);
            for (int i = 0; i < columnsSize; i++) {  
                map.put("favorites", c.getString(0));   
                 
            }  
            listItems.add(map);  
        } 

		SimpleAdapter adapter = new SimpleAdapter(getActivity(), listItems,
				R.layout.favorites_listview, new String[] { "favorites" },
				new int[] { R.id.favorites_textview });
		setListAdapter(adapter);

	}

}