package com.jsquareddevelopment.cabit;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import org.json.JSONObject;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SimpleAdapter;
import android.widget.TextView;

public class TaxiCompanies extends Fragment implements OnClickListener {

	public static String phoneNumber;

	ImageButton saveButton, infoButton;
	CheckBox selectAllTaxiCompaniesCheckBox, selectCurrentCompany;
	ImageButton callButton;
	Thread timer;
	ListView mListView;
	LinearLayout companyLinearLayout;
	TextView companyName, companyPhoneNumber;

	String strUrl = "http://cabitus.com/app/get_taxi_companies_info.php";

	public TaxiCompanies() {
	};

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.taxi_companies_fragment,
				container, false);

		DownloadTask downloadTask = new DownloadTask();
		downloadTask.execute(strUrl);
		declarations(rootView);

		return rootView;
	}

	public void declarations(View view) {
		saveButton = (ImageButton) view.findViewById(R.id.saveButton);
		infoButton = (ImageButton) view.findViewById(R.id.infoButton);
		mListView = (ListView) view.findViewById(R.id.taxiCompaniesListView);
		selectAllTaxiCompaniesCheckBox = (CheckBox) view
				.findViewById(R.id.selectAllTaxiCompaniesCheckBox);

		saveButton.setOnClickListener(this);
		infoButton.setOnClickListener(this);
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
		case R.id.saveButton:
			saveButton.setImageResource(R.drawable.saved);

			final Handler handler = new Handler();

			Timer timer = new Timer();
			timer.schedule(new TimerTask() {
				public void run() {
					handler.post(new Runnable() {
						public void run() {
							saveButton.setImageResource(R.drawable.save);
						}
					});
				}
			}, 2500);
			break;
		case R.id.selectAllTaxiCompaniesCheckBox:
			/**
			 * Open up a dialog to show information about the selected taxi
			 * company
			 * */

			break;
		default:
			break;
		}

	}

	private String downloadUrl(String strUrl) throws IOException {
		String data = "";
		InputStream iStream = null;
		try {
			URL url = new URL(strUrl);
			HttpURLConnection urlConnection = (HttpURLConnection) url
					.openConnection();
			urlConnection.connect();
			iStream = urlConnection.getInputStream();
			BufferedReader br = new BufferedReader(new InputStreamReader(
					iStream));
			StringBuffer sb = new StringBuffer();

			String line = "";
			while ((line = br.readLine()) != null) {
				sb.append(line);
			}

			data = sb.toString();

			br.close();

		} catch (Exception e) {
			Log.d("Exception while downloading url", e.toString());
		} finally {
			iStream.close();
		}

		return data;
	}

	private class DownloadTask extends AsyncTask<String, Integer, String> {
		String data = null;

		@Override
		protected String doInBackground(String... url) {
			try {
				data = downloadUrl(url[0]);

			} catch (Exception e) {
				Log.d("Background Task", e.toString());
			}
			return data;
		}

		@Override
		protected void onPostExecute(String result) {
			ListViewLoaderTask listViewLoaderTask = new ListViewLoaderTask();
			listViewLoaderTask.execute(result);
		}
	}

	private class ListViewLoaderTask extends
			AsyncTask<String, Void, SimpleAdapter> {

		JSONObject jObject;

		@Override
		protected SimpleAdapter doInBackground(String... strJson) {
			try {
				jObject = new JSONObject(strJson[0]);
				JSONParser urlJsonParser = new JSONParser();
				urlJsonParser.parse(jObject);
			} catch (Exception e) {
				Log.d("JSON Exception1", e.toString());
			}

			JSONParser urlJsonParser = new JSONParser();
			List<HashMap<String, Object>> names = null;

			try {
				names = urlJsonParser.parse(jObject);
			} catch (Exception e) {
				Log.d("Exception", e.toString());
			}

			String[] from = { "name" };
			int[] to = { R.id.name };

			MyCustomAdapter adapter = new MyCustomAdapter(getActivity()
					.getApplicationContext(), names,
					R.layout.taxi_companies_list_view, from, to);

			return adapter;
		}

		@Override
		protected void onPostExecute(SimpleAdapter adapter) {
			mListView.setAdapter(adapter);
		}
	}

	public class MyCustomAdapter extends SimpleAdapter implements
			OnClickListener {
		private final Context context;

		public MyCustomAdapter(Context context,
				List<HashMap<String, Object>> names, int taxiCompaniesListView,
				String[] from, int[] to) {
			super(context, names, taxiCompaniesListView, from, to);
			this.context = context;
		}

		@SuppressLint("ViewHolder")
		@SuppressWarnings("unchecked")
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View view = inflater.inflate(R.layout.taxi_companies_list_view,
					parent, false);
			mListViewDeclarations(view);

			for (int i = 0; i < getCount(); i++) {
				HashMap<String, Object> hm = (HashMap<String, Object>) getItem(position);
				// companyPhoneNumber.setText("P: " +
				// hm.get("companyPhoneNumber").toString());
				companyName.setText(hm.get("name").toString());
				companyPhoneNumber.setText(hm.get("companyPhoneNumber")
						.toString());
			}

			return view;
		}

		private void mListViewDeclarations(View view) {
			companyName = (TextView) view.findViewById(R.id.name);
			companyPhoneNumber = (TextView) view.findViewById(R.id.phoneNumber);
			selectCurrentCompany = (CheckBox) view
					.findViewById(R.id.selectCurrentCompany);
			callButton = (ImageButton) view.findViewById(R.id.callButton);

			callButton.setOnClickListener(this);

			// CheckAll/UncheckAll
			selectAllTaxiCompaniesCheckBox
					.setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View v) {
							for (int i = 0; i < mListView.getChildCount(); i++) {
								RelativeLayout itemLayout = (RelativeLayout) mListView
										.getChildAt(i);
								CheckBox cb = (CheckBox) itemLayout
										.findViewById(R.id.selectCurrentCompany);

								if (cb.isChecked()
										&& selectAllTaxiCompaniesCheckBox
												.isChecked()) {
									cb.setChecked(true);
								}

								if (cb.isChecked()
										&& !selectAllTaxiCompaniesCheckBox
												.isChecked()) {
									cb.setChecked(false);
								}

								if (!cb.isChecked()
										&& selectAllTaxiCompaniesCheckBox
												.isChecked()) {
									cb.setChecked(true);
								}
							}
						}
					});
		}

		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.callButton:
				phoneNumber = (String) companyPhoneNumber.getText();
				Intent callIntent = new Intent(Intent.ACTION_CALL);

				// for (int i = 0; i < mListView.getChildCount(); i++) {
				// RelativeLayout itemLayout = (RelativeLayout)
				// mListView.getChildAt(i);
				// TextView tv = (TextView)
				// itemLayout.findViewById(R.id.phoneNumber);
				// int size = tv.getText().length();
				// phoneNumber = tv.getText().subSequence(3, size).toString();
				// }

				Log.e("phoneNumber", "" + phoneNumber);
				callIntent.setData(Uri.parse("tel:" + phoneNumber));
				startActivity(callIntent);
				break;
			default:
				break;
			}
		}

	}

}