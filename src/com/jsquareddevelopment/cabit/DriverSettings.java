package com.jsquareddevelopment.cabit;

import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceActivity;

public class DriverSettings extends PreferenceActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public void onBackPressed() {
		Intent i = new Intent("com.jsquareddevelopment.cabit.DRIVERHOMESCREEN");
		startActivity(i);
		super.onBackPressed();
	}
}
