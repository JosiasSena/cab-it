/*
 * @author: Xun Cui
 * user can store their favorites addresses here and delete them
 * connect with addToFavorites_actions in onOptionsItemSelected of MainActivity
 * 
 */
package com.jsquareddevelopment.cabit;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

public class Favorites extends ListFragment {

	ArrayList<String> myLines = new ArrayList<String>();
	ArrayAdapter<String> myAdapter;
	static String favoriteString;
	TextView favoritesTextView;
	SQLiteDatabase db;
	UserDataDBHelper dbHelper;
	UserDataDBManager dbManager;
	ListView myListView;

	public Favorites() {
	};

	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.favorites_fragment,
				container, false);

		return rootView;

	}

	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
	
		//Create database here
		dbHelper = new UserDataDBHelper(getActivity());
		db = dbHelper.getWritableDatabase();
		dbHelper.onCreate(db);
		dbManager = new UserDataDBManager(dbHelper, db);
		//display the data stored in sql
		final List<Map<String, Object>> listItems = new ArrayList<Map<String, Object>>();
		Cursor c = db.rawQuery(
				"SELECT favoritesAddress_column FROM favoritesAddress_table",
				null);
		int columnsSize = c.getColumnCount();
		while (c.moveToNext()) {
			HashMap<String, Object> map = new HashMap<String, Object>();
			for (int i = 0; i < columnsSize; i++) {
				map.put("favorites", c.getString(0));

			}
			listItems.add(map);
		}
		//Adapter
		final SimpleAdapter adapter = new SimpleAdapter(getActivity(),
				listItems, R.layout.favorites_listview,
				new String[] { "favorites" },
				new int[] { R.id.favorites_textview });
		setListAdapter(adapter);
		myListView = getListView();
		
		// set touch listener, to dismiss data with swipe gesture
		SwipeDismissListViewTouchListener touchListener = new SwipeDismissListViewTouchListener(
				myListView,
				new SwipeDismissListViewTouchListener.DismissCallbacks() {
					@Override
					public boolean canDismiss(int position) {
						return true;
					}

					@Override
					public void onDismiss(ListView listView,
							int[] reverseSortedPositions) {
						for (int position : reverseSortedPositions) {
							
							//acquire string from the gesture position
							HashMap<String, Object> map = (HashMap<String, Object>) myListView
									.getItemAtPosition(position);
							String addresString = map.get("favorites").toString();

							Log.e("delete string ", addresString);
							//delete it from the database
							dbManager.deletefavorites(addresString);
							// dbManager.deletefavorites(addresString);
							listItems.remove(position);
						}
						adapter.notifyDataSetChanged();
					}
				});
		myListView.setOnTouchListener(touchListener);
		// Setting this scroll listener is required to ensure that during
		// ListView scrolling,
		// we don't look for swipes.
		myListView.setOnScrollListener(touchListener.makeScrollListener());

		myListView.setOnItemClickListener(new OnItemClickListener() {

			//set on click listener for furture use
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				HashMap<String, String> map = (HashMap<String, String>) myListView
						.getItemAtPosition(position);
				String goHereAddress = map.get("favorites");
				Log.e("goHereAddress get from the listview", goHereAddress);
				Toast.makeText(getActivity(),
						"Here we are going to " + goHereAddress,
						Toast.LENGTH_SHORT).show();
				SharedPreferences goHerePreferences = getActivity()
						.getSharedPreferences(null, Context.MODE_PRIVATE);
				SharedPreferences.Editor editor = goHerePreferences.edit();
				editor.putString("goHereAddress", goHereAddress);
				editor.commit();
				editor.apply();
				// Here we start the new fragment or activity to call a taxi;
				/*
				 * FragmentTransaction transaction = fm.beginTransaction();
				 * goHereFragment = new goHereFragment();
				 * transaction.replace();
				 * transaction.commit();
				 */
			}

		});

	}
}
