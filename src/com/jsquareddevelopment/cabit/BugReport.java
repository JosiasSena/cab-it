package com.jsquareddevelopment.cabit;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.security.PublicKey;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.Toast;

public class BugReport extends Activity implements OnClickListener {

	EditText bugReportDetails, otherEditTextView;
	ImageButton brImage1, brImage2, brImage3, submitBugReportButton;

	Spinner moduleSpinner;
	ArrayAdapter<String> moduleAdapter;
	String mExtraString;
	public static String module;

	private static final String username = "sys-admin@jsquareddevelopment.com";
	private static final String password = "yFfoaO,ks}.*";
	public static int RESULT_LOAD_IMAGE = 1;
	public static String picturePath;

	File pic;
	List<String> attachment_PathList = new ArrayList<String>();
	Bitmap resizedBitmap;
	Cursor SelectedCursor;
	final Intent GaleryIntent = new Intent(Intent.ACTION_PICK,
			android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.bugreport);
		setRefs();
	}

	public void setRefs() {
		bugReportDetails = (EditText) findViewById(R.id.bugReportDetails);
		otherEditTextView = (EditText) findViewById(R.id.otherEditTextView);

		submitBugReportButton = (ImageButton) findViewById(R.id.submitBugReportButton);
		brImage1 = (ImageButton) findViewById(R.id.brImage1);
		brImage2 = (ImageButton) findViewById(R.id.brImage2);
		brImage3 = (ImageButton) findViewById(R.id.brImage3);

		brImage1.setOnClickListener(this);
		brImage2.setOnClickListener(this);
		brImage3.setOnClickListener(this);

		/** Spinner Start */
		// Populate array from XML resources
		Resources res = this.getResources();
		String[] moduleStrings = res.getStringArray(R.array.bugreport_spinner);

		moduleAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, moduleStrings);
		moduleSpinner = (Spinner) findViewById(R.id.moduleSpinner);
		moduleSpinner.setAdapter(moduleAdapter);

		moduleSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				if (moduleAdapter.getItem(position).toString()
						.equalsIgnoreCase("other")) {
					otherEditTextView.setEnabled(true);
					otherEditTextView.setInputType(InputType.TYPE_CLASS_TEXT);
					module = otherEditTextView.getText().toString();
				} else {
					otherEditTextView.setEnabled(false);
					otherEditTextView.setInputType(InputType.TYPE_NULL);
					module = moduleAdapter.getItem(position).toString();
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> av) {
			}
		});

		/** Spinner Finish */

		submitBugReportButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (otherEditTextView.isEnabled()
						&& otherEditTextView.getText() != null) {
					module = otherEditTextView.getText().toString();
				}

				new Thread(new Runnable() {
					public void run() {
						runOnUiThread(new Runnable() {
							@Override
							public void run() {
								if (bugReportDetails.getText().toString()
										.matches("")) {
									Toast.makeText(getApplicationContext(),
											"Please enter details",
											Toast.LENGTH_SHORT).show();
								}
								if (!bugReportDetails.getText().toString()
										.matches("")) {
									sendMail(
											"sys-admin@jsquareddevelopment.com",
											"Bug Report", "<b>Module: </b>"
													+ module
													+ " <br />"
													+ "<b>Details: </b>"
													+ bugReportDetails
															.getEditableText()
															.toString());
								}
							}
						});
					}
				}).start();
			}
		});
	}

	@Override
	protected void onActivityResult(int RequestCode, int ResultCode, Intent data) {
		super.onActivityResult(RequestCode, ResultCode, data);

		if (RequestCode == RESULT_LOAD_IMAGE && ResultCode == RESULT_OK
				&& data != null) {
			Uri SelectedImage = data.getData();
			String[] FilePathColumn = { MediaStore.Images.Media.DATA };

			SelectedCursor = getContentResolver().query(SelectedImage,
					FilePathColumn, null, null, null);
			SelectedCursor.moveToFirst();

			int columnIndex = SelectedCursor.getColumnIndex(FilePathColumn[0]);
			picturePath = SelectedCursor.getString(columnIndex);
			attachment_PathList.add(picturePath);

			try {
				resizedBitmap = Bitmap.createScaledBitmap(
						BitmapFactory.decodeFile(picturePath), 140, 140, true);

				if (mExtraString != null) {
					if (getExtra().equals("brImage1")) {
						brImage1.setImageBitmap(resizedBitmap);
					} else if (getExtra().equals("brImage2")) {
						brImage2.setImageBitmap(resizedBitmap);
					} else if (getExtra().equals("brImage3")) {
						brImage3.setImageBitmap(resizedBitmap);
					}

				} else {
					Log.e("Extras", "Intent Extra is null");
				}

			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			}
			SelectedCursor.close();
		}
	}

	/****** Send mail methods start ******/
	private void sendMail(String email, String subject, String messageBody) {
		javax.mail.Session session = createSessionObject();

		try {
			Message message = createMessage(email, subject, messageBody,
					session);
			new SendMailTask().execute(message);
		} catch (AddressException e) {
			e.printStackTrace();
		} catch (MessagingException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	}

	private Message createMessage(String email, String subject,
			String messageBody, javax.mail.Session session)
			throws MessagingException, UnsupportedEncodingException {

		Multipart multipart = new MimeMultipart("mixed");

		/** Define message */
		BodyPart messageBodyPart = new MimeBodyPart();
		MimeMessage message = new MimeMessage(session);

		message.setFrom(new InternetAddress(username, "Bug Report"));
		message.addRecipient(Message.RecipientType.TO, new InternetAddress(
				email, email));
		message.setSubject(subject);
		message.setSentDate(new Date());
		message.setText(messageBody);
		messageBodyPart.setContent(messageBody, "text/html");

		multipart.addBodyPart(messageBodyPart);

		/** Handle Attachments */
		for (String str : attachment_PathList) {
			BodyPart attachmentBodyPart = new MimeBodyPart();
			DataSource source = new FileDataSource(str);
			attachmentBodyPart.setDataHandler(new DataHandler(source));
			attachmentBodyPart.setFileName(source.getName());
			multipart.addBodyPart(attachmentBodyPart);
		}

		message.setContent(multipart);
		return message;
	}

	private javax.mail.Session createSessionObject() {
		Properties properties = new Properties();
		properties.put("mail.smtp.auth", "true");
		properties.put("mail.smtp.starttls.enable", "true");
		properties.put("mail.smtp.host", "69.195.124.89"); // Host
		properties.put("mail.smtp.port", "26"); // Port

		return javax.mail.Session.getInstance(properties, new Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		});
	}

	private class SendMailTask extends AsyncTask<Message, Void, Void> {
		private ProgressDialog progressDialog;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progressDialog = ProgressDialog.show(BugReport.this, "Please wait",
					"Sending bug report...", true, false);
		}

		@Override
		protected void onPostExecute(Void aVoid) {
			super.onPostExecute(aVoid);
			progressDialog.dismiss();
			Toast.makeText(BugReport.this, "Bug report sent!",
					Toast.LENGTH_SHORT).show();
		}

		@Override
		protected Void doInBackground(Message... messages) {
			try {
				Transport.send(messages[0]);
			} catch (MessagingException e) {
				e.printStackTrace();
			}
			return null;
		}
	}

	/****** Send mail methods finish ******/

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
		case R.id.brImage1:
			brImage1();
			break;
		case R.id.brImage2:
			brImage2();
			break;
		case R.id.brImage3:
			brImage3();
			break;
		default:
			break;
		}
	}

	public void brImage1() {
		new Thread(new Runnable() {
			public void run() {
				runOnUiThread(new Runnable() {
					@Override
					public void run() {
						saveExtra("brImage1");
						startActivityForResult(GaleryIntent, RESULT_LOAD_IMAGE);
					}
				});
			}
		}).start();
	}

	public void brImage2() {
		new Thread(new Runnable() {
			public void run() {
				runOnUiThread(new Runnable() {
					@Override
					public void run() {
						saveExtra("brImage2");
						startActivityForResult(GaleryIntent, RESULT_LOAD_IMAGE);
					}
				});
			}
		}).start();
	}

	public void brImage3() {
		new Thread(new Runnable() {
			public void run() {
				runOnUiThread(new Runnable() {
					@Override
					public void run() {
						saveExtra("brImage3");
						startActivityForResult(GaleryIntent, RESULT_LOAD_IMAGE);
					}
				});
			}
		}).start();
	}

	public void saveExtra(String name) {
		mExtraString = name;
	}

	public String getExtra() {
		return mExtraString;
	}
}
